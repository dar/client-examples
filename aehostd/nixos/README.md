Nixpkgs for aehostd
===================

This package builds and installs aehostd on [NixOS](https://nixos.org/).

Maintained by [](mailto:).

See also:
  * [Nixpkgs Users and Contributors Guide](https://nixos.org/nixpkgs/manual/)
