{ config, pkgs, lib, ... }:

let
  aehostd = (import ./aehostd.nix);
  aehostd_conf = "${./aehostd.conf}";
  nss_pam_ldapd_aehostd = pkgs.nss_pam_ldapd.overrideAttrs (oldAttrs: {
    configureFlags = [
      "--with-bindpw-file=/run/nslcd/bindpw"
      # "--with-nslcd-socket=/run/nslcd/socket"
      "--with-nslcd-pidfile=/run/nslcd/nslcd.pid"
      "--with-pam-seclib-dir=$(out)/lib/security"
      "--enable-kerberos=no"

      "--with-module-name=aedir"
      "--disable-nslcd"
      "--disable-pynslcd"
      "--disable-kerberos"
      "--disable-utils"
      "--with-nss-maps=passwd,group,hosts"
      "--with-nslcd-socket=/run/aehostd/aehostd.sock"
    ];
    postInstall = "";
  });
in
{
  services.openssh.enable = true;
  services.openssh.permitRootLogin = "yes";
  services.timesyncd.enable = lib.mkDefault true;

  services.zerotierone.enable = true;
  services.zerotierone.joinNetworks = [
    # put zerotierone network ids here
  ];

  environment.systemPackages = with pkgs; [
    vim htop dnsutils inetutils
    aehostd
  ];

  users.mutableUsers = false;
  users.users.root.password = "yolo";

  system.nssModules = [ nss_pam_ldapd_aehostd ];
  system.nssDatabases = {
    passwd = [ "aedir" ];
    group = [ "aedir" ];
  };

  systemd.services.aehostd = {
    after = [
      "local-fs.target"
      "network.target"
    ];
    before = [
      "nss-user-lookup.target"
    ];
    description = "NSS/PAM service for AE-DIR";
    documentation = [
      "https://ae-dir.com/aehostd.html"
    ];
    requires = [
      "local-fs.target"
      "network.target"
    ];
    wantedBy = [
      "multi-user.target"
    ];
    environment = {
      LDAPNOINIT = "1";
      PYTHONHASHSEED = "random";
      PYTHONOPTIMIZE = "2";
      PYTHONDONTWRITEBYTECODE = "1";
      PYTHONWARNINGS = "error";
    };
    serviceConfig = rec {
      Type = "simple";
      PIDFile = "/run/aehostd/aehostd.pid";
      User = "aehostd";
      Group = User;
      ExecStart = "${aehostd}/bin/aehostd -m aehostd.srv --no-fork --pid ${PIDFile} --config ${aehostd_conf}";
      Restart = "always";
      StandardError = "syslog";
      UMask = "027";
      CapabilityBoundingSet = "";
      AmbientCapabilities = "";
      PrivateTmp = "yes";
      PrivateDevices = "yes";
      DevicePolicy = "closed";
      DeviceAllow = "/dev/log w";
      ProtectSystem = "full";
      ProtectHome = "yes";
      ProtectKernelModules = "yes";
      ProtectKernelTunables = "yes";
      ProtectKernelLogs = "yes";
      ProtectControlGroups = "yes";
      ProtectHostname = "yes";
      ProtectClock = "yes";
      NoNewPrivileges = "yes";
      MountFlags = "private";
      LockPersonality = "yes";
      RestrictRealtime = "yes";
      RestrictNamespaces = "yes";
      RestrictSUIDSGID = "yes";
      KeyringMode = "private";
      MemoryDenyWriteExecute = "yes";
      RemoveIPC = "yes";
      SystemCallArchitectures = "native";
      SystemCallFilter = "~ madvise @clock @cpu-emulation @debug @keyring @module @mount @raw-io @reboot @swap @obsolete @timer @resources @privileged @pkey @obsolete @setuid";
      RestrictAddressFamilies = "AF_INET AF_INET6 AF_UNIX";
    };
  };

  systemd.services.aehostd-ph = {
    after = [
      "local-fs.target"
    ];
    description = "Privileged helper service for AE-DIR";
    documentation = [
      "https://ae-dir.com/aehostd.html"
    ];
    requires = [
      "local-fs.target"
    ];
    wantedBy = [
      "multi-user.target"
    ];
    environment = {
      PYTHONHASHSEED = "random";
      PYTHONOPTIMIZE = "2";
      PYTHONDONTWRITEBYTECODE = "1";
      PYTHONWARNINGS = "error";
    };
    serviceConfig = rec {
      Type = "simple";
      PIDFile = "/run/aehostd/aehostd-ph.pid";
      User = "root";
      Group = "root";
      ExecStart = "${aehostd}/bin/aehostd-ph --no-fork --pid ${PIDFile} --config ${aehostd_conf}";
      Restart = "always";
      StandardError = "syslog";
      UMask = "027";
      CapabilityBoundingSet = "CAP_DAC_OVERRIDE CAP_FOWNER CAP_CHOWN";
      AmbientCapabilities = "CAP_DAC_OVERRIDE CAP_FOWNER CAP_CHOWN";
      PrivateTmp = "yes";
      PrivateDevices = "yes";
      DevicePolicy = "closed";
      DeviceAllow = "/dev/log w";
      PrivateNetwork = "yes";
      IPAddressDeny = "any";
      ProtectSystem = "yes";
      ProtectHome = "yes";
      ProtectKernelModules = "yes";
      ProtectKernelTunables = "yes";
      ProtectKernelLogs = "yes";
      ProtectControlGroups = "yes";
      ProtectHostname = "yes";
      ProtectClock = "yes";
      NoNewPrivileges = "yes";
      MountFlags = "private";
      LockPersonality = "yes";
      RestrictRealtime = "yes";
      RestrictNamespaces = "yes";
      RestrictSUIDSGID = "yes";
      KeyringMode = "private";
      MemoryDenyWriteExecute = "yes";
      RemoveIPC = "yes";
      SystemCallArchitectures = "native";
      SystemCallFilter = "~ madvise @clock @cpu-emulation @debug @keyring @module @mount @raw-io @reboot @swap @obsolete @timer @resources @privileged @pkey @obsolete @setuid @chown";
      RestrictAddressFamilies = "AF_UNIX";
    };
  };
}
