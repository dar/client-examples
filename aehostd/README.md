Build and install aehostd
=========================

Platform-specific build and install files for using
[aehostd](https://www.ae-dir.com/aehostd.html) on:

  * Debian Buster and newer
  * CentOS 7 and newer
  * NixOS
