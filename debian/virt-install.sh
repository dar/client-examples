QEMU_URL="qemu:///system"

REPO_URL="http://ftp.de.debian.org/debian/dists/stable/main/installer-amd64/"

VTYPE="kvm"
VNET="virtnet1"
MAC_ADDRESS="52:54:00:23:42:02"

virt-install \
  --connect="${QEMU_URL}" \
  --virt-type "${VTYPE}" \
  --name ae-client-deb \
  --cpu=host \
  --memory 512 \
  --disk path=/var/lib/libvirt/images/ae-client-deb.qcow2,format=qcow2,bus=virtio,size=6\
  --boot hd \
  --network network=${VNET},mac=${MAC_ADDRESS} \
  --location="${REPO_URL}" \
  --extra-args "auto" \
  --initrd-inject=preseed-ae-client.txt
